<?php
/*
Plugin Name: Team - Lemonade Stand
Plugin URI: https://www.lemonadestand.org/
Description: Team custom post type.
Version: 1.0
Author: Lemonade Stand
Author URI: https://www.lemonadestand.org/
*/

include("inc/post_type.php");
include("inc/shortcodes.php");
include("inc/taxonomy.php");


/* rename labels */
function lemonade_team_title_alter( $title ) {
    $screen = get_current_screen();
    if ( $screen->post_type == "team") {
        $title = 'Enter Question Here';
    }
    return $title;
}
add_filter( 'enter_title_here', 'lemonade_team_title_alter' );

