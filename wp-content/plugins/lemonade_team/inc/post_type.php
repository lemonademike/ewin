<?php
function lemonade_create_team() {
	$labels = array(
		'name'                => 'Team',
		'singular_name'       => 'Team',
		'menu_name'           => 'Team',
		'parent_item_colon'   => 'Parent Team:',
		'all_items'           => 'All Team Members',
		'view_item'           => 'View Team',
		'add_new_item'        => 'Add New Team Member',
		'add_new'             => 'New Team Member',
		'edit_item'           => 'Edit Team',
		'update_item'         => 'Update Team',
		'search_items'        => 'Search Team',
		'not_found'           => 'No Team found',
		'not_found_in_trash'  => 'No Team found in Trash',
	);

	$args = array(
		'label'               => 'Team',
		'description'         => 'Team post type',
		'labels'              => $labels,
		'hierarchical'        => true,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 6,
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'capability_type'     => 'page',
		'supports'            => array( 'title', 'excerpt', 'thumbnail', 'revisions', 'page-attributes', 'editor'),
		'menu_icon' => plugins_url( 'lemonade_icon.png', __FILE__ ),
	);

	register_post_type( 'team', $args );
}
add_action( 'init', 'lemonade_create_team', 0 );