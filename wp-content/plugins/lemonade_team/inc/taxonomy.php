<?php
//Team Type
function team_type_init() {
	register_taxonomy(
		'team_type',
		'team',
		array(
			'label' => __( 'Team Type' ),
			'rewrite' => array( 
			'slug' => 'team_type',
			),
		'hierarchical' => true,
		)
	);
}
add_action( 'init', 'team_type_init' ); 