<?php
//Service Type
function service_type_init() {
	register_taxonomy(
		'service_type',
		'services',
		array(
			'label' => __( 'Service Type' ),
			'rewrite' => array( 
			'slug' => 'service_type',
			),
		'hierarchical' => true,
		)
	);
}
add_action( 'init', 'service_type_init' ); 