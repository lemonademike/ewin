<?php
/*
Template Name: Team
*/
?>


	<article <?php post_class(); ?>>

		<div class="container">
			<div class="row">
				<div class="col-sm-6">
					<div class="team-image">
						<?php
							if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
							the_post_thumbnail( 'full' );
							}
							?>
					</div>
				</div>

				<div class="col-sm-6">
					<div class="content-info">
						
							<h2 class="title text-left"><?php echo $post->post_title; ?></h2>
							<!-- <?php get_template_part('templates/entry-meta'); ?> -->
						
						<div class="team-content">
							<?php echo $post->post_content; ?>
						</div>
					</div>
				</div>
			</div>
		</div>

	</article>
