<?php
/* Shortcodes */
function lemonade_team_shortcode( $atts ) {
	extract( shortcode_atts( array(
		'order'				=> 'ASC',
		'orderby'			=> 'menu_order',
		'meta_key'			=> '',
		'posts_per_page'	=> '-1', 
		'display'			=> 'standard',
		'terms_list' 		=> '',
		'by_term'			=> '0',
		'team_type' 		=> '',
	), $atts ) );

	$terms_array = explode(",", $team_type);

	$db_args = array(
		'post_type' 		=> 'team',
		'order'				=> $order,
		'orderby'			=> $orderby,
		'meta_key'			=> $meta_key,
		'posts_per_page' 	=> $posts_per_page, 
		'by_term'			=> $by_term,
		'team_type' 		=> $team_type,
	);


	if($team_type != ""){
		$db_args['tax_query'][] = array(
			array(
				'taxonomy' => 'team_type',
				'field'    => 'slug',
				'terms'    => $terms_array,
			),
		);		
	}

	$team_loop = new WP_Query( $db_args );

	$content = '';

	if($team_loop->have_posts()) {
		switch($display) {	

		case "accordion":
			$content .= '<div class="panel-group" id="team-accordion">';

			$i = 0;

			while( $team_loop->have_posts() ) : $team_loop->the_post();
			
				if($i == 0) { $first = "in"; } else { $first = ''; }
			
				$content_filtered = get_the_content();
				$content_filtered = apply_filters('the_content', $content_filtered);
				$content_filtered = str_replace(']]>', ']]&gt;', $content_filtered);
				
			    $content .= '<div class="panel panel-default">';
				$content .= '<div class="panel-heading">';
				$content .= '<h3 class="panel-title">';
				$content .= '<a class="accordion-toggle" data-toggle="collapse" data-parent="#team-accordion" href="#collapse'.$i.'">';
				$content .= get_the_title();
				$content .= '</a>';					
				$content .= '</h3>';
				$content .= '</div>'; // end .panel-heading
				$content .= '<div id="collapse'.$i.'" class="panel-collapse collapse '.$first.'">';
				$content .= '<div class="panel-body">'.$content_filtered.'</div>';
				$content .= '</div>'; // end .collapse
				$content .= '</div>'; // end .panel

				$i++;

			endwhile;

			$content .= "</div>"; // end .panel-group

			break;


			case "izimodal":

				$content .= '<div class="team-wrapper">';

				$i = 1; 

				while( $team_loop->have_posts() ) : $team_loop->the_post();

					$content_filtered = get_the_content();
					$content_filtered = apply_filters('the_content', $content_filtered);
					$content_filtered = str_replace(']]>', ']]&gt;', $content_filtered);

					//echo '<pre>';
					//var_dump($team_loop);
					//echo '</pre>'; 

					$fields = get_fields();
				
					/*
					$content .= '<div class="team-single">';
					$content .= '<a href="" class="trigger" data-iziModal-open="#modal'.$i.'">';
					$content .= get_the_post_thumbnail($team_loop->ID, 'team-thumb');
					$content .= '<p>'.get_the_title().'</p>';
					$content .= '</a>';
					$content .= '</div>'; 
					*/

					$content .= '<div class="team-single">';
					$content .= 	'<div class="team-inner">';
					$content .= 		'<a href="" class="trigger" data-iziModal-open="#'.$team_type.'-modal'.$i.'">';
					$content .= 			'<div class="team-thumb">'.get_the_post_thumbnail($team_loop->ID, 'team-thumb').'</div>';
					$content .= 			'<div class="team-caption">';
					$content .= 				'<h4 class="name">'.get_the_title().'</h4>';
					$content .= 				'<div class="inner-more">';
					$content .= 					'<h5 class="position">'.$fields["position_title"].'</h5>';
					$content .= 					'<a class="learn-more" href="#">Learn More <i class="fa fa-arrow-right" aria-hidden="true"></i>';
					$content .= 				'</div>';
					$content .= 			'</div>'; 
					$content .= 		'</a>';
					$content .= 	'</div>'; 
					$content .= '</div>'; 

 
					$content .= '<div id="'.$team_type.'-modal'.$i.'" class="iziModal">';
					$content .= 	'<div class="media">';
					$content .= 		'<div class="grid-container">';

					$content .= 			'<div class="img-block">';
					$content .= 				'<div class="profile-image">'; 
					$content .= 					get_the_post_thumbnail($team_loop->ID, 'team-modal');
					$content .= 				'</div>';
					$content .= 			'</div>';

					$content .= 			'<div class="text-block">';
					$content .= 				'<h3 class="name">'.get_the_title().'</h3>'; 
					$content .= 				'<h4 class="position">'.$fields["position_title"].'</h4>';					
					$content .= 				'<div class="media-body">';
					$content .= 					'<p>'.$content_filtered.'</p>';
					$content .= 				'</div>';
					$content .= 			'</div>';

					$content .= 		'</div>';
					$content .= 	'</div>';
					$content .= '</div>';   

					$i++;

				endwhile;

				$content .= '</div>';

				break;


			case "standard":

				$content .= '<div class="team-wrapper">';

				while( $team_loop->have_posts() ) : $team_loop->the_post();

					$content .= '<div class="medium-6 large-3 columns">';
					$content .= '<div class="team-single">';
					$content .= '<a href="'.get_permalink().'">';
					$content .= get_the_post_thumbnail($team_loop->ID, 'doc-thumb');
					$content .= '<p>'.get_the_title().'</p>';
					$content .= '</a>';
					$content .= '</div>';
					$content .= '</div>';

				endwhile;

				$content .= '</div>';

				break;



			case "content":

				$content .= '<div class="team-wrapper">';

				while( $team_loop->have_posts() ) : $team_loop->the_post();

					$content_filtered = get_the_content();
					$content_filtered = apply_filters('the_content', $content_filtered);
					$content_filtered = str_replace(']]>', ']]&gt;', $content_filtered);

					$content .= '<div class="team-single">';
					$content .= '<h3 class="team-title">'.get_the_title().'</h3>';
					$content .= '<div class="team-content">'.$content_filtered.'</div>';
					$content .= '</div>';
				endwhile;

				$content .= '</div>';

				break;

			case "excerpt":

				$content .= '<div class="team-wrapper">';

				while( $team_loop->have_posts() ) : $team_loop->the_post();
					$content .= '<div class="team-single">';
					$content .= '<h3 class="team-title"><a href=".get_permalink().">'.get_the_title().'</a></h3>';
					$content .= '<div class="team-excerpt">'.get_the_excerpt().'</div>';
					$content .= '</div>';
				endwhile;

				$content .= '</div>';

				break;

			case "list":

				$content .= '<ul class="team-wrapper">';

				while( $team_loop->have_posts() ) : $team_loop->the_post();
					$content .= '<li class="team-single">';
					$content .= '<a class="team-title" href=".get_permalink().">'.get_the_title().'</a>';
					$content .= '</li>';
				endwhile;

				$content .= '</ul>';

				break;



				case "meet-the-team":

				$content .= '<div class="team-container">';

				while( $team_loop->have_posts() ) : $team_loop->the_post();
					
					$content .= '<div class="team-single text-center">';
					$content .= '<a class="category-thumb" href="'.get_permalink().'">'.get_the_post_thumbnail(get_the_ID(), 'category-thumb' ).'</a>';
					$content .= '<h3 class="team-title highlight-primary">'.get_the_title().'</h3>';
					$content .= '<div class="team-excerpt">'.get_the_excerpt().'</div>';
					$content .= '<a class="btn btn-primary" href="'.get_permalink().'">Read More <i class="fa fa-angle-double-right"></i></a>';
					$content .= '</div>';
					
				endwhile;

				$content .= '</div>';

				break;
		}
			
	}

	wp_reset_postdata();
	return $content;
}
add_shortcode( 'lemonade_team', 'lemonade_team_shortcode' );